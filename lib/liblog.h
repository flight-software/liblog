#ifndef LIBLOG_H
#define LIBLOG_H

#include <stdint.h>

#include "libdll.h"

/**
 * \brief Logging tool
 *
 * It's like a cslog, but more space efficient and easier to parse
 *
 * We only log one element, not two, since the architecture is working towards
 * sending static requests and logging variable responses
 *
 * The log struct below (everything but FILE*) is written to the start of the
 * file in the order given, since everything is one byte, big/little endian
 * does not matter, as well as a 64-bit microsecond timestamp (when it was created)
 *
 * Every element has a microsecond offset since the last element, and 'len' byte of
 * data
 *
 * sys_id is the daemon that's currently running, and type is an identifier for a
 * log
 *
 * Path for all logs is "/var/log/cdh/logs/", and the filename format is 
 * [sys_id]_[type]_[start UNIX timestamp]
 *
 * We log to a file for five minutes, then we create a new one. There is no garbage
 * collection on old files
 *
 * ==========
 *
 * As a side, this can also generate CSVs, but that's not the normal mode of
 * operation
 *
 * This can also create a linked list inside it, where it stores the last N
 * elements logged, because I need to seek back a few samples for averaging,
 * and having one unit that functions as a log and a linked list makes things
 * easier, since the APIs for both are pretty similar
 */

#define LOG_FORMAT_UNDEF 0
#define LOG_FORMAT_U8 1
#define LOG_FORMAT_U16 2
#define LOG_FORMAT_U32 3
#define LOG_FORMAT_U64 4
#define LOG_FORMAT_FLOAT 5
#define LOG_FORMAT_DOUBLE 6

#define LOG_MODE_NATIVE 1
#define LOG_MODE_CSV 2

typedef struct {
    dll_fs list;
    
    FILE *file;
    
    uint64_t last_append_milli_s;
    uint64_t min_wait_milli_s;
    
    uint64_t max_entry_cnt;
    uint64_t entry_cnt;
 
    size_t history_len;
    size_t history_cnt;
    
    uint16_t elem_cnt;
    
    char sys_name[5]; // 4 letter name, then NULL byte
    uint8_t type;

    uint8_t format;
    uint8_t log_mode;

    uint8_t overwrite;
} log_fs;

typedef struct {
    log_fs** logs;
    size_t num_logs;
} log_rpc_shim_fs;

#define LOG_PATH "/var/log/cdh/logs"

int log_init(log_fs* log, const char* sys_name, uint8_t type, 
                    uint8_t format, uint16_t elem_cnt, uint8_t log_mode,
                    size_t history_len, uint64_t min_wait_milli_s);
int log_close(log_fs* log);
int log_modify_overwrite(log_fs* log, uint64_t max_entry_cnt, uint8_t overwrite);
int log_modify_rate(log_fs* log, uint64_t min_wait_milli_s);
int log_add(log_fs* log, const void* data);
// TODO: May drop data. Not thread safe.
int log_rpc_shim(const void*, size_t, void*, size_t*, void* logs);
int log_split(log_fs* log);

#endif
