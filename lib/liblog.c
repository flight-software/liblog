#include <stdint.h>
#include <string.h>
#include <inttypes.h>
#include <errno.h>
#include <time.h>
#include <sys/stat.h>

#include "libdebug.h"
#include "libdll.h"
#include "liblog.h"

static uint64_t get_time_milli(void)
{
   struct timespec tv;
   clock_gettime(CLOCK_REALTIME, &tv);
   uint64_t sec = tv.tv_sec;
   uint64_t nsec = tv.tv_nsec;
   return sec * 1000 + nsec / 1000000;
}

static int create_filename(const log_fs* log_, char* dest, size_t max_len)
{
    if(snprintf(dest, max_len, "/var/log/cdh/logs/%s_%"PRIu64"_%"PRIu64, log_->sys_name, (uint64_t)log_->type, (uint64_t)time(NULL)) < 0) {
        int old_errno = errno;
        P_ERR("snprintf failed: sys_name (%s), type (%"PRIu64"), time (%"PRIu64")\n errno: %d (%s)", log_->sys_name, (uint64_t)log_->type, (uint64_t)time(NULL),
                                                                                                     old_errno, strerror(old_errno));
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

/**
 * \brief Reload the file log_
 *
 * Call this on initialization, and afterwards every five minues
 * 
 * Since we deal with millisecond precision, we can't guarantee that 
 * the start time in milliseconds is exactly five minutes from the
 * previous
 */

static int log_load_fd(log_fs* log_)
{
    if(!log_) return EXIT_FAILURE;

    if(!log_->file){
        char filename[256];
        filename[0] = 0;

        // give everyone R/W/X
        if(mkdir(LOG_PATH, S_IRWXU | S_IRWXG | S_IRWXO) && errno != EEXIST) {
            P_ERR("Failed to make directory, errno: %d (%s)", errno, strerror(errno));
            return EXIT_FAILURE;
        }

        if(create_filename(log_, filename, sizeof(filename))) {
            P_ERR_STR("failed to create log file");
            return EXIT_FAILURE;
        }

        log_->file = fopen(filename, "w");
        if(!log_->file) return EXIT_FAILURE;
        errno = 0; // errno gets set to 17 sometimes, but the file opens properly... (Dillon 09/06/2018)
    }
    return EXIT_SUCCESS;
}

static int log_reload_fd(log_fs* log_)
{
    if(!log_) return EXIT_FAILURE;

    if(log_->file) {
        log_->file = freopen(NULL, "w", log_->file);
    } else {
        return log_load_fd(log_);
    }

    if(!log_->file) {
        P_ERR("Failed to freopen log for %s (subsys: %d), errno: %d (%s)", log_->sys_name, (int)log_->type, errno, strerror(errno));
        errno = 0;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

/**
 * \brief Initialize logging
 */
int log_init(log_fs *log_, const char* sys_name, uint8_t type, uint8_t format, uint16_t elem_cnt, uint8_t log_mode, size_t history_len, uint64_t min_wait_milli_s)
{
    if(!log_) {
        P_ERR_STR("log is null");
        return EXIT_FAILURE;
    }
    
    memset(log_, 0, sizeof(log_fs));

    memcpy(log_->sys_name, sys_name, sizeof(log_->sys_name) - 1);
    
    log_->type = type;

    log_->history_len = history_len;
    log_->history_cnt = 0;
    
    log_->format = format;
    log_->elem_cnt = elem_cnt;
    log_->log_mode = log_mode;

    log_->last_append_milli_s = 0;
    log_->min_wait_milli_s = min_wait_milli_s;

    if(log_load_fd(log_)) return EXIT_FAILURE;
    
    if(dll_init(&(log_->list), NULL)) return EXIT_FAILURE;

    return EXIT_SUCCESS;
}

int log_close(log_fs* log_)
{
    if(!log_) {
        P_ERR_STR("log is null");
        return EXIT_FAILURE;
    }
   
    int ret = EXIT_SUCCESS;
    {
        if(fclose(log_->file)) {
            P_ERR("Failed to close log file: errno %d (%s)", errno, strerror(errno));
            ret = EXIT_FAILURE;
        }
        log_->file = NULL;
    }
    {
        if(dll_close(&log_->list)) ret = EXIT_FAILURE;
    }

    return ret;
}

int log_modify_overwrite(log_fs* log_, uint64_t max_entry_cnt, uint8_t overwrite)
{
    if(!log_) {
        P_ERR_STR("log is null");
        return EXIT_FAILURE;
    }
    
    log_->max_entry_cnt = max_entry_cnt;
    log_->entry_cnt = 0;
    log_->overwrite = overwrite ? 1 : 0;
    
    if(log_->overwrite) {
        return log_reload_fd(log_);
    }

    return EXIT_SUCCESS;
}

int log_modify_rate(log_fs* log_, uint64_t min_wait_milli_s)
{
    if(!log_) {
        P_ERR_STR("log is null");
        return EXIT_FAILURE;
    }
    
    log_->min_wait_milli_s = min_wait_milli_s;
    
    return EXIT_SUCCESS;
}

static size_t log_len_from_format(uint8_t format)
{
    switch(format) {
    case LOG_FORMAT_DOUBLE:
        return sizeof(double);
        break;
    case LOG_FORMAT_FLOAT:
        return sizeof(float);
        break;
    case LOG_FORMAT_U64:
        return sizeof(uint64_t);
        break;
    case LOG_FORMAT_U32:
        return sizeof(uint32_t);
        break;
    case LOG_FORMAT_U16:
        return sizeof(uint16_t);
        break;
    case LOG_FORMAT_U8:
        return sizeof(uint8_t);
        break;
    default:
      P_ERR_STR("Format isn't valid");
      return 1;
      break;
    }
    return 1;
}

static int log_refresh_file(log_fs* log_)
{
    if(!log_->file) {
        if(log_load_fd(log_)) return EXIT_FAILURE;
    } else if(log_->overwrite && log_->entry_cnt >= log_->max_entry_cnt) {
        if(log_reload_fd(log_)) return EXIT_FAILURE;
        log_->entry_cnt = 0;
    }

    return EXIT_SUCCESS;
}

static int log_add_to_dll(log_fs* log_, const void* data)
{
    const size_t datatype_size = log_len_from_format(log_->format);
 
    void* data_ = calloc(datatype_size, log_->elem_cnt);

    if(!data_) {
        P_ERR("calloc broke, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    memcpy(data_, data, datatype_size * log_->elem_cnt);
 
    if(dll_push_head(&(log_->list), data_, datatype_size * log_->elem_cnt, 0)) {
        P_ERR_STR("Failed to push new data into dll");
        free(data_);
        return EXIT_FAILURE;
    }

    log_->history_cnt++;

    return EXIT_SUCCESS;
}

static uint8_t rate_limit(const log_fs* log_, uint64_t curr_time_millis)
{
    if(curr_time_millis < (log_->last_append_milli_s + log_->min_wait_milli_s)) return 1;
    return 0;
}

static int log_native(const log_fs* log_, uint64_t curr_time_milli_s, const void* data)
{
    if(fwrite(&curr_time_milli_s, sizeof(curr_time_milli_s), 1, log_->file) != 1) {
        P_ERR_STR("Can't write timestamp to file");
        return EXIT_FAILURE;
    }
    if(fwrite(data, log_len_from_format(log_->format), log_->elem_cnt, log_->file) != log_->elem_cnt) {
        P_ERR_STR("Can't write datagram to file");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

static int log_csv(const log_fs* log_, uint64_t curr_time_milli_s, const void* data)
{
    fprintf(log_->file, "%"PRIu64",", curr_time_milli_s);

    switch(log_->format) {
    case LOG_FORMAT_U8:
        if(log_->elem_cnt != 1) {
            P_ERR_STR("Cannot log arbitrary byte arrays to a CSV");
            return EXIT_FAILURE;
        }
        fprintf(log_->file, "%"PRIu16",", *(const uint8_t*)data);
        break;
    case LOG_FORMAT_U16:
        for(size_t i = 0; i < log_->elem_cnt; i++){
            fprintf(log_->file, "%d,", ((const uint16_t*)data)[i]);
        }
        break;
    case LOG_FORMAT_FLOAT:
        for(size_t i = 0; i < log_->elem_cnt; i++) {
            fprintf(log_->file, "%f,", (double)(((const float*)data)[i]));
        }
        break;
    case LOG_FORMAT_DOUBLE:
        for(size_t i = 0; i < log_->elem_cnt; i++){
            fprintf(log_->file, "%lf,", ((const double*)data)[i]);
        }
        break;
    default:
        P_ERR_STR("No type specified for printf delimiter, and log_ging to CSV");
        return EXIT_FAILURE;
    }
    
    fprintf(log_->file, "\n");

    return EXIT_SUCCESS;
}

int log_add(log_fs* log_, const void* data)
{
    if(!log_) {
        P_ERR_STR("log is null");
        return EXIT_FAILURE;
    }

    if(log_refresh_file(log_)) return EXIT_FAILURE;

    if(log_->history_cnt == log_->history_len){
        free(dll_pull_tail(&(log_->list)));
        log_->history_cnt--;
    }

    if(log_add_to_dll(log_, data)) return EXIT_FAILURE;
    
    const uint64_t cur_time_milli_s = get_time_milli();
   
    if(rate_limit(log_, cur_time_milli_s)) return EXIT_SUCCESS;

    if(log_->log_mode == LOG_MODE_NATIVE) {
        if(log_native(log_, cur_time_milli_s, data)) return EXIT_FAILURE;
    } else if(log_->log_mode == LOG_MODE_CSV) {
        if(log_csv(log_, cur_time_milli_s, data)) return EXIT_FAILURE;
    } else {
        P_ERR_STR("Invalid logging mode");
        return EXIT_FAILURE;
    }

    if(fflush(log_->file)) {
        P_ERR_STR("Can't flush file contents to disk");
        return EXIT_FAILURE;
    }

    log_->last_append_milli_s = cur_time_milli_s;
 
    if(log_->overwrite) log_->entry_cnt += 1;
 
    return EXIT_SUCCESS;
}

int log_rpc_shim(const void* in, size_t in_size, void* out, size_t* out_size, void* logs)
{
    (void)in; (void)in_size; (void)out;
    
    if(out_size) *out_size = 0;
    
    log_rpc_shim_fs* to_split = logs;
    
    int err = EXIT_SUCCESS;
    
    for(size_t i = 0; i < to_split->num_logs; i++) {
        int r = log_split(to_split->logs[i]);
        
        if(r == EXIT_FAILURE && err == EXIT_SUCCESS) {
            err = EXIT_FAILURE;
        }
    }

    return err;
}

// TODO: May drop data. Not thread safe.
int log_split(log_fs* log_)
{
    if(!log_) {
        P_ERR_STR("log is null");
        return EXIT_FAILURE;
    }
    
    if(log_->file != NULL) fclose(log_->file);
    
    log_->file = NULL;
    
    return log_load_fd(log_);
}

